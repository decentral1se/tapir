# TAPir: Tiny Anonymous Peer

![](tapir.png)

Tapir is a small library for building p2p applications over anonymous communication systems (right now Tapir only supports Tor v3 Onion Services).

Tapir has been designed as a replacement to the Ricochet protocol.

## Features

* New Authentication Protocol based on v3 Onion Services
* Bidirectional Application Channels.

**Work In Progress**