package persistence

import (
	"os"
	"testing"
)

func TestBoltPersistence_Open(t *testing.T) {
	os.Remove("test.dbgi")
	db := new(BoltPersistence)
	db.Open("test.dbgi")
	db.Setup([]string{"tokens"})
	// 2020.02: Fails in WSL1 because of a mmap issue.
	// https://github.com/microsoft/WSL/issues/4873
	// Scheduled to be fixed in the 20h1 Win10 release
	db.Persist("tokens", "random_value", true)

	var exists bool
	db.Load("tokens", "random_value", &exists)

	if exists {
		t.Logf("Successfully stored: %v", exists)
	} else {
		t.Fatalf("Failure to store record in DB!")
	}
	db.Close()
}
