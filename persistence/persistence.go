package persistence

// Service provides a consistent interface for interacting with on-disk, in-memory or server-backed storage
type Service interface {
	Open(handle string) error
	Setup(buckets []string) error
	Persist(bucket string, name string, value interface{}) error
	Check(bucket string, name string) (bool, error)
	Load(bucket string, name string, value interface{}) error
	Close()
}
