package auditable

import (
	"fmt"
	"git.openprivacy.ca/cwtch.im/tapir/persistence"
	"git.openprivacy.ca/cwtch.im/tapir/primitives"
	"git.openprivacy.ca/openprivacy/log"
	"os"
	"testing"
)

func BenchmarkAuditableStore(b *testing.B) {
	log.SetLevel(log.LevelDebug)
	os.Remove("benchmark-auditablestore.db")

	as := new(Store)
	serverID, _ := primitives.InitializeEphemeralIdentity()
	as.Init(serverID)

	db := new(persistence.BoltPersistence)
	db.Open("benchmark-auditablestore.db")

	as.LoadFromStorage(db)

	for i := 0; i < b.N; i++ {
		data := fmt.Sprintf("Message %v", i)
		as.Add(Message(data))
	}
	db.Close()
	db.Open("benchmark-auditablestore.db")
	vs := new(Store)
	vs.Init(serverID)
	vs.LoadFromStorage(db)
	db.Close()
	os.Remove("benchmark-auditablestore.db")
}

func TestAuditableStore(t *testing.T) {
	as := new(Store)
	vs := new(Store)

	serverID, _ := primitives.InitializeEphemeralIdentity()
	as.Init(serverID)
	vs.Init(serverID) // This doesn't do anything

	as.Add([]byte("Hello World"))
	state := as.GetState()

	if vs.MergeState(state) != nil {
		t.Fatalf("Fraud Proof Failed on Honest Proof")
	}

	fraudProof := as.Add([]byte("Hello World 2"))

	// If you comment these out it simulates a lying server.
	state = as.GetState()
	if vs.MergeState(state) != nil {
		t.Fatalf("Fraud Proof Failed on Honest Proof")
	}

	fraud, err := vs.VerifyFraudProof(as.LatestCommit, fraudProof, serverID.PublicKey())

	if err != nil {
		t.Fatalf("Error validated fraud proof: %v", err)
	}

	if fraud {
		t.Fatalf("Technically a fraud, but the client hasn't updated yet")
	}
}
