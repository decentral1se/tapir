package primitives

import (
	"crypto/subtle"
	"testing"
)

func TestIdentity_EDH(t *testing.T) {

	id1, _ := InitializeEphemeralIdentity()
	id2, _ := InitializeEphemeralIdentity()

	k1, err1 := id1.EDH(id2.PublicKey())
	k2, err2 := id2.EDH(id1.PublicKey())

	if err1 == nil && err2 == nil && subtle.ConstantTimeCompare(k1, k2) == 1 {
		t.Logf("k1: %x\nk2: %x\n", k1, k2)
	} else {
		t.Fatalf("The derived keys should be identical")
	}

}

func BenchmarkEDH(b *testing.B) {
	id1, _ := InitializeEphemeralIdentity()
	id2, _ := InitializeEphemeralIdentity()

	for i := 0; i < b.N; i++ {
		k1, err1 := id1.EDH(id2.PublicKey())
		k2, err2 := id2.EDH(id1.PublicKey())
		if err1 == nil && err2 == nil && subtle.ConstantTimeCompare(k1, k2) == 1 {
			//b.Logf("k1: %x\nk2: %x\n", k1, k2)
		} else {
			b.Fatalf("The derived keys should be identical")
		}
	}
}
