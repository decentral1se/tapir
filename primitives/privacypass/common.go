package privacypass

// Transcript Constants
const (
	BatchProofProtocol = "privacy-pass-batch-proof"
	BatchProofX        = "X-batch"
	BatchProofY        = "Y-batch"
	BatchProofPVector  = "P-vector"
	BatchProofQVector  = "Q-vector"

	DLEQX = "X"
	DLEQY = "Y"
	DLEQP = "P"
	DLEQQ = "Q"
	DLEQA = "A"
	DLEQB = "B"
)
